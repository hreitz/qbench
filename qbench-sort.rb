#!/usr/bin/ruby

def print_help()
    $stderr.puts("#{__FILE__} <results>")
end

args = ARGV.to_a

to_sort = args.shift
if !to_sort
    print_help()
    exit 1
end

if args.length > 0
    print_help()
    exit 1
end

to_sort = IO.read(to_sort).split("#{$/}#{$/}").map { |result| result.split($/).map { |line| line.strip } }

to_sort.sort! do |x, y|
    Float(y[1].split[0]) <=> Float(x[1].split[0])
end

max_len = to_sort[0][1].split[0].length

to_sort.each do |result|
    puts("#{' ' * (max_len - result[1].split[0].length)}#{result[1]} – #{result[0]}")
end

#!/usr/bin/ruby

require 'json'
require 'shellwords'

class Array
    def avg
        self.inject(:+) / self.length.to_f
    end

    def variance
        a = self.avg
        self.map { |v| (v - a) * (v - a) }.inject(:+) / (self.length - 1).to_f
    end

    def sd
        self.variance ** 0.5
    end
end

def run_test(test, count, simultaneous)
    prog = test['prog']
    args = test['args'].dup

    vars = {
        'COUNT' => count.to_s,
        'SIMULTANEOUS' => simultaneous.to_s,
    }
    if test['json-vars']
        test['json-vars'].each do |var, value|
            vars[var] = JSON.generate(value)
        end
    end
    if test['plain-vars']
        test['plain-vars'].each do |var, value|
            vars[var] = value.to_s
        end
    end

    args.map! do |arg|
        if arg.kind_of?(String)
            arg = arg.dup
        else
            arg = JSON.generate(arg)
        end

        vars.each do |var, value|
            arg.gsub!('"$' + var + '"', value)
            arg.gsub!('$' + var, value)
        end

        arg
    end

    result = `#{prog.shellescape} #{args.map { |arg| arg.shellescape } * ' '}`.strip
    if result.empty?
        return 0.0 # Allow killing on hangs
    end

    case test['result-style']
    when 'qsd-rs'
        parts = result.split
        if parts[-1] == 'IOPS'
            Float(parts[-2])
        else
            time = parts[-1]
            if time =~ /\dms$/
                time = Float(time.sub(/ms$/, '')) * 1.0e-3
            elsif time =~ /\ds$/
                time = Float(time.sub(/s$/, ''))
            else
                $stderr.puts("Unrecognized time unit on '#{time}'")
                exit 1
            end
            count.to_f / time
        end
    when 'qemu-img'
        count.to_f / Float(result.split($/)[-1].split[-2])
    else
        $stderr.puts("Unrecognized result style “#{test['result-style']}”")
        exit 1
    end
end

cull_outliers = false
out_file = nil
config = nil

def print_help()
    $stderr.puts("Usage: #{__FILE__} <config.json> [--cull-outliers] [out_file]")
    $stderr.puts('(Passing --cull-outliers is NOT recommended and will give you worthless results.)')
end

ARGV.each do |arg|
    case arg
    when '--cull-outliers'
        cull_outliers = true
    when '-h', '--help'
        print_help()
        exit 0
    else
        if !config
            config = JSON.parse(IO.read(arg))
        else
            if File.exist?(arg)
                $stderr.puts("“#{arg}” already exists, aborting")
                exit 1
            end
            out_file = File.open(arg, 'w')
        end
    end
end

if !config
    print_help()
    exit 1
end

config['run'].each do |test_name|
    if !config['tests'][test_name]
        $stderr.puts("No such test name: “#{test_name}”")
        exit 1
    end
    test = config['tests'][test_name].dup

    while test['templates'] && !test['templates'].empty?
        template_name = test['templates'].shift
        if !config['templates'][template_name]
            $stderr.puts("No such template name: “#{template_name}”")
            exit 1
        end
        test.merge!(config['templates'][template_name])
    end

    count = test['count']
    simultaneous = test['simultaneous']

    if !simultaneous
        smt_runs = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]

        smt_runs = smt_runs.map { |simultaneous|
            result = 3.times.map { run_test(test, count / 4, simultaneous) }.avg

            $stdout.write("\r\e[KTested #{simultaneous} simultaneously: #{'%.3f' % result} IOPS")
            $stdout.flush()
            [simultaneous, result]
        }
        simultaneous = smt_runs.inject([nil, 0.0]) { |best, x| x[1] > best[1] ? x : best }[0]

        $stdout.write("\r\e[K")
        $stdout.flush()
    end

    print_str = "[#{test_name}] #{count} requests, with #{simultaneous} simultaneously"
    puts(print_str)
    if out_file
        out_file.puts(print_str)
    end

    iops = (test['warmup-iterations'] + test['test-iterations']).times.map { |i|
        $stdout.write("\r\e[K#{i - test['warmup-iterations']}/#{test['test-iterations']}")
        $stdout.flush()

        run_test(test, count, simultaneous)
    }[test['warmup-iterations']..]

    if cull_outliers
        threshold = 0.01

        best_array = iops.dup
        best_ratio = best_array.sd / best_array.avg
        cull_it = 0

        while iops.sd > threshold * iops.avg
            avg = iops.avg
            old_ratio = iops.sd / iops.avg

            old_entry = iops.each_with_index.inject([avg, nil]) { |worst, x| (x[0] - avg).abs > (worst[0] - avg).abs ? x : worst }
            old_time = old_entry[0]
            old_i = old_entry[1]

            result = run_test(test, count, simultaneous)
            iops[old_i] = result

            ratio = iops.sd / iops.avg
            if ratio < best_ratio
                best_array = iops.dup
                best_ratio = ratio
            end
            $stdout.write("\r\e[K#{'%.2f %%' % (ratio * 100.0)}")
            $stdout.flush()

            cull_it += 1
            if cull_it >= test['test-iterations'] * 2
                iops = best_array
                break
            end
        end
    end

    if test['test-iterations'] > 0
        puts("\r\e[K  #{'%12.3f' % iops.avg} ±#{'%.3f' % iops.sd} IOPS\n\n")
        if out_file
            out_file.puts("  #{'%12.3f' % iops.avg} ±#{'%.3f' % iops.sd} IOPS\n\n")
        end
    end
end
